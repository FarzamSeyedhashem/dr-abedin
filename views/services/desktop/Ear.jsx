import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from "@material-ui/core/Divider";


const styles = theme => ({
    root: {
        '&>p': {
            margin: 0,
            marginBottom: 16,
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.88,
            letterSpacing: 'normal',
            color: '#212121',
        },
        "&>h3": {
            fontFamily: 'Shabnam',
            fontSize: 18,
            fontWeight: 'bold',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.44,
            letterSpacing: 'normal',
            color: '#212121',
        },
        '&>ul': {
            padding: 0,
            listStyle: 'none',
            '&>li': {
                marginBottom: 16,
                display: 'flex',
                fontFamily: 'Shabnam',
                fontSize: 16,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.63,
                letterSpacing: 'normal',
                color: '#757575',
                '&>span:nth-child(1)': {
                    marginRight: 22,
                    borderRadius: '9px',
                    backgroundColor: '#123574',
                    width: 18,
                    height: 18,
                }
            }
        }
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                    آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه
                    ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد</p>
                <Divider/>
                <h3>خدماتی که در مطب دکتر بهزاد عابدین در ارتباط بابیماری های گوشی انجتم می شود</h3>
                <ul>
                    <li>
                        <span/>
                        <span>
                                        فارغ التحصيل پزشكي عمومي از دانشگاه علوم پزشكي ايران در سال ١٣٨٧ با رتبه برتر
                                        </span>
                    </li>
                    <li>
                        <span/>
                        <span>
                                        فارغ التحصيل دوره تخصصي گوش، حلق، بيني و جراحي سر و گردن از دانشگاه علوم پزشكي
                                        شهيد بهشتي در سال ١٣٩١
                                        </span>
                    </li>
                    <li>
                        <span/>
                        <span>
                                        داراي درجه دانشنامه ( بورد تخصصي )
                                        </span>
                    </li>
                    <li>
                        <span/>
                        <span>
                                        عضو انجمن گوش، حلق، بيني و جراحي سر و گردن ايران و عضو انجمن راینولوژی ایران
                                        </span>
                    </li>
                </ul>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
