import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {Hidden} from "@material-ui/core";
import DesktopPage from "./desktop/Index";
import MobilePage from "./mobile/Index";
import Layout from "../../layouts/Main";

const styles = theme => ({
    root: {},
});

class Main extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes,posts,lang} = this.props;
        return (
            <div>
                <Hidden smDown>
                    <DesktopPage lang={lang} posts={posts}/>
                </Hidden>
                <Hidden mdUp>
                    <MobilePage lang={lang} posts={posts}/>
                </Hidden>
            </div>
        );
    }
}

Main.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Main);
