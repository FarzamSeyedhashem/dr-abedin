import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Ear from "./Ear";
import ReactMarkdown from "react-markdown";


const styles = theme => ({
    root: {
        marginTop:56,
    },
    buttonGroup: {
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:40,
        marginBottom:40,
        borderRadius: 8,
        border: 'solid 1px #123574',
        '&>button': {
            padding: '14px 40px',
            fontFamily: 'Shabnam',
            fontSize: 20,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.3,
            letterSpacing: 'normal',
            color: '#757575',
        }
    },
    selectButton: {
        fontWeight: 'bold !important',
        color: '#ffffff !important',
    }
});

class LearnCard extends React.Component {

    state = {
        checked: 0,
    };

    handleExpandClick = () => {

    };
    handleButtonClick = (id) => {
        this.setState({
            checked: id
        })
    }


    render() {
        const {classes, posts, lang} = this.props;
        const post = posts.services.find(item => item.lang.name === lang);
        return (
            <div className={classes.root}>
                <Container>
                    <video controls style={{width:'100%',height:480,objectFit:'cover',borderRadius:12,marginBottom:16,marginTop:24}}>
                        <source src="/video.MP4" type="video/mp4"/>
                    </video>
                    <ButtonGroup className={classes.buttonGroup} color="primary"
                                 aria-label="outlined primary button group">
                        <Button className={this.state.checked === 0 && classes.selectButton}
                                onClick={() => this.handleButtonClick(0)}
                                variant={this.state.checked === 0 && "contained"}>گوش</Button>
                        <Button className={this.state.checked === 1 && classes.selectButton}
                                onClick={() => this.handleButtonClick(1)}
                                variant={this.state.checked === 1 && "contained"}>حلق</Button>
                        <Button className={this.state.checked === 2 && classes.selectButton}
                                onClick={() => this.handleButtonClick(2)}
                                variant={this.state.checked === 2 && "contained"}>بینی</Button>
                    </ButtonGroup>
                    <div className={classes.detail}>
                        {this.state.checked === 0 &&
                        <ReactMarkdown source={post.ear}/>
                        }
                        {this.state.checked === 1 &&
                        <ReactMarkdown source={post.throat}/>
                        }
                        {this.state.checked === 2 &&
                        <ReactMarkdown source={post.nose}/>
                        }
                    </div>

                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
