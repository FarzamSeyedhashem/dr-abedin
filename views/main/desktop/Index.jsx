import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ReactMarkdown from "react-markdown";
import ReactHtmlParser from 'react-html-parser';

const styles = theme => ({
    root: {
        '&>img': {
            width: '100%',
            height: '480px',
            objectFit: 'cover',
            marginBottom: 55,
        }
    },
    detail: {
        paddingTop: 82,
        paddingBottom: 67,
        marginBottom: 56,
        '&>div': {
            '&>div': {
                '&>img': {
                    borderRadius: '99%',
                    width: '220px',
                    height: '220px',
                    objectFit: 'cover',
                },
                '&>h1': {
                    marginBottom: 20,
                    margin: 0,
                    padding: 0,
                    fontFamily: 'Shabnam',
                    fontSize: 26,
                    fontWeight: 'bold',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1,
                    letterSpacing: 'normal',
                    color: '#212121',
                },
                '&>ul': {
                    padding: 0,
                    listStyle: 'none',
                    position: 'relative',

                    '&>li': {
                        '&::before': {
                            content: "''",
                            borderRadius: 18,
                            backgroundColor: '#123574',
                            height: '18px',
                            fontWeight: 'bold',
                            display: 'inline-block',
                            width: '18px',
                            marginRight: 16,
                            color: '#123574',
                        },
                        marginBottom: 16,
                        display: 'flex',
                        fontFamily: 'Shabnam',
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontStretch: 'normal',
                        fontStyle: 'normal',
                        lineHeight: 1.63,
                        letterSpacing: 'normal',
                        color: '#757575',
                        '&>span:nth-child(1)': {
                            marginRight: 22,
                            borderRadius: '9px',
                            backgroundColor: '#123574',
                            width: 18,
                            height: 18,
                        }
                    }
                }
            }
        },
    },
    card: {
        paddingTop: 56,
        paddingBottom: 56,
        width: '100%',
        textAlign: 'center',
        '&>img': {
            objectFit: 'contain',
            marginBottom: 32,
        },
        '&>h3': {
            margin: 0,
            marginBottom: 27,
            fontFamily: 'Shabnam',
            fontSize: 24,
            fontWeight: 'bold',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.38,
            textAlign: 'center',
            letterSpacing: 'normal',
            color: '#727272',
        },
        '&>p': {
            margin: 0,
            marginBottom: 28,
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.86,
            letterSpacing: 'normal',
            textAlign: 'center',
            color: '#757575',
        },
        '&>button': {
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 300,
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.38,
            letterSpacing: 'normal',
        }
    },
    question: {
        // marginTop: 56,
        // marginBottom: 89,
        '&>div': {
            marginBottom: 60,
            '&>div': {
                '&>img': {
                    marginTop: 47,
                    width: '243.9px',
                    height: '242.2px',
                    objectFit: 'cover',
                },
                "&>h2": {
                    marginTop: 79,
                    marginBottom: 26,
                    fontFamily: 'Shabnam',
                    fontSize: 24,
                    fontWeight: 'bold',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                },
                '&>p': {
                    margin: 0,
                    marginBottom: 24,
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 2,
                    letterSpacing: 'normal',
                    color: '#212121',
                },
                '&>button': {
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 300,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                }
            }
        }

    },
    reserv: {
        '&>div': {
            paddingBottom: 56,
            paddingTop: 56,
            '&>div': {
                '&>img': {
                    marginTop: 47,
                    width: '272.1px',
                    height: 'auto',
                    objectFit: 'cover',
                },
                "&>h2": {
                    marginTop: 56,
                    marginBottom: 26,
                    fontFamily: 'Shabnam',
                    fontSize: 24,
                    fontWeight: 'bold',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                },
                '&>p': {
                    margin: 0,
                    marginBottom: 24,
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 2,
                    letterSpacing: 'normal',
                    color: '#212121',
                },
                '&>button': {
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 300,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                }
            }
        }

    }
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };

    componentDidMount() {
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
        })
    }


    render() {
        const {classes, posts, lang} = this.props;
        console.log(posts)
        const post = posts.main.find(item => item.lang.name === lang);
        console.log(post)
        return (
            typeof post !== "undefined" && <div className={classes.root}>

                <div style={{width: '100%', height: 480, position: 'relative'}} className="swiper-container">
                    <div style={{position: 'absolute', top: 16, right: 16, zIndex: 70,}}><img width="220px"
                                                                                              src="/logo.png"/></div>
                    <div className="swiper-wrapper">
                        <div className="swiper-slide" style={{width: '100%', height: 480}}><img
                            style={{width: '100%', height: 480, objectFit: 'cover'}} src={"/assets/main-top.jpg"}/>
                        </div>
                        <div className="swiper-slide" style={{width: '100%', height: 480}}><img
                            style={{width: '100%', height: 480, objectFit: 'cover'}} src={"/assets/main-top.jpg"}/>
                        </div>
                    </div>

                    <div className="swiper-pagination"></div>
                </div>
                <Container>
                    <div className={classes.detail}>
                        <Grid spacing={4} container justify={"space-evenly"} alignItems={'center'}>
                            <Grid item md={"auto"}>
                                <img src={"/assets/dr.jpg"}/>
                            </Grid>
                            <Grid item md>
                                <h1 style={{fontFamily: 'Nastaligh'}}>{post.drabedintitle}</h1>
                                {post.drabedin && ReactHtmlParser(post.drabedin)}
                            </Grid>
                        </Grid>
                    </div>
                </Container>
                <div style={{backgroundColor: '#f2f2f2'}}>
                    <Container>
                        <Grid container justify={"space-evenly"} alignItems={"center"}>
                            <Grid md={3}>
                                <div className={classes.card}>
                                    <img width={"158.8px"} height={"216.5px"} src={"/assets/ear.svg"}/>
                                    <h3>گوش</h3>
                                    <p>
                                        {post.ear && ReactHtmlParser(post.ear)}
                                    </p>
                                    <Button component={'a'} href={'/' + lang + '/services#ear'} variant={"outlined"}
                                            color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                            <Grid md={3}>
                                <div className={classes.card}>
                                    <img width={"192px"} height={"200px"} src={"/assets/throat.svg"}/>
                                    <h3>حلق</h3>
                                    <p>
                                        {post.throat && <ReactMarkdown source={post.throat}/>}
                                    </p>
                                    <Button component={'a'} href={'/' + lang + '/services#throat'} variant={"contained"}
                                            color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                            <Grid md={3}>
                                <div className={classes.card}>
                                    <img width={"152.4px"} height={"200px"} src={"/assets/nose.svg"}/>
                                    <h3>بینی</h3>
                                    <p>
                                        {post.nose && <ReactMarkdown source={post.nose}/>}
                                    </p>
                                    <Button component={'a'} href={'/' + lang + '/services#nose'} variant={"outlined"}
                                            color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Container>
                </div>
                <Container>
                    <div className={classes.question}>
                        <Divider/>
                        <Grid container justify={"space-evenly"} alignItems={"center"}>
                            <Grid item md={3}>
                                <img src="/assets/question.svg"/>
                            </Grid>
                            <Grid item md={7}>
                                <h2>{post.questiontitle}</h2>
                                <p>
                                    {post.question && ReactHtmlParser(post.question)}
                                </p>
                                <Button variant={"outlined"} color={"primary"}>
                                    پرسیدن سوال
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider/>
                    </div>
                </Container>
                <div style={{backgroundColor: '#f2f2f2'}}>
                    <Container>
                        <div className={classes.reserv}>
                            <Grid container justify={"space-evenly"} alignItems={"center"}>
                                <Grid item md={7}>
                                    <h2>{post.reservetitle}</h2>
                                    <p>
                                        {post.reserve && ReactHtmlParser(post.reserve)}
                                    </p>
                                    <Button variant={"outlined"} color={"primary"}>نوبت دهی</Button>
                                </Grid>
                                <Grid item md={3}>
                                    <img src="/assets/computer.svg"/>
                                </Grid>
                            </Grid>
                            {/*<Divider/>*/}
                        </div>
                    </Container>
                </div>


            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
