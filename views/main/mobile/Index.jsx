import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ReactHtmlParser from "react-html-parser";
import ReactMarkdown from "react-markdown";


const styles = theme => ({
    root: {
        '&>img': {
            marginTop: 56,
            width: '100%',
            height: 164,
            objectFit: 'cover',
        }
    },
    dr: {
        textAlign: 'center',
        '&>img': {
            marginBottom: 20,
            width: 80,
            height: 80,
            objectFit: 'cover',
            marginTop: 20,
            borderRadius: 40,
        },
        '&>p': {
            margin: 0,
            marginBottom: 16,
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 2.14,
            letterSpacing: 'normal',
            textAlign: 'center',
            color: '#424242',
        }

    },
    card: {
        width: '100%',
        textAlign: 'center',
        '&>img': {
            objectFit: 'contain',
            marginBottom: 32,
        },
        '&>h3': {
            margin: 0,
            marginBottom: 27,
            fontFamily: 'Shabnam',
            fontSize: 24,
            fontWeight: 'bold',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.38,
            textAlign: 'center',
            letterSpacing: 'normal',
            color: '#727272',
        },
        '&>p': {
            margin: 0,
            marginBottom: 28,
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.86,
            letterSpacing: 'normal',
            textAlign: 'center',
            color: '#757575',
        },
        '&>button': {
            marginBottom: 50,
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 300,
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.38,
            letterSpacing: 'normal',
        }
    },
    question: {
        marginTop: 0,
        marginBottom: 89,
        '&>div': {
            marginBottom: 60,
            textAlign: 'center',
            '&>div': {
                '&>img': {
                    marginTop: 32,
                    width: '243.9px',
                    height: '242.2px',
                    objectFit: 'cover',
                },
                "&>h2": {
                    textAlign: 'center',
                    marginTop: 40,
                    marginBottom: 26,
                    fontFamily: 'Shabnam',
                    fontSize: 24,
                    fontWeight: 'bold',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                },
                '&>p': {
                    textAlign: 'center',
                    margin: 0,
                    marginBottom: 24,
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 2,
                    letterSpacing: 'normal',
                    color: '#212121',
                },
                '&>button': {
                    textAlign: 'center',
                    fontFamily: 'Shabnam',
                    fontSize: 16,
                    fontWeight: 300,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.38,
                    letterSpacing: 'normal',
                    color: '#123574',
                }
            }
        }

    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes, posts, lang} = this.props;
        const post = posts.main.find(item => item.lang.name === lang);

        return (
            <div className={classes.root}>
                <img src={"/assets/main-top.jpg"}/>
                <Container>
                    <div className={classes.dr}>
                        <img src={"/assets/dr.jpg"}/>
                        <h1 style={{fontFamily: 'Yekan'}}>{post.drabedintitle}</h1>
                        <p> {post.drabedin && ReactHtmlParser(post.drabedin)}</p>
                    </div>
                </Container>
                <Divider/>
                <div style={{backgroundColor: '#f2f2f2',paddingTop:24}}>
                    <Container>
                        <Grid container justify={"center"} alignItems={"center"}>
                            <Grid xs={12}>
                                <div className={classes.card}>
                                    <img width={"158.8px"} height={"216.5px"} src={"/assets/ear.svg"}/>
                                    <h3>گوش</h3>
                                    <p>
                                        {post.ear && ReactHtmlParser(post.ear)}
                                    </p>
                                    <Button variant={"outlined"} color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                            <Grid xs={12}>
                                <div className={classes.card}>
                                    <img width={"192px"} height={"200px"} src={"/assets/throat.svg"}/>
                                    <h3>حلق</h3>
                                    <p>
                                        {post.throat && <ReactMarkdown source={post.throat}/>}
                                    </p>
                                    <Button variant={"contained"} color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                            <Grid xs={12}>
                                <div className={classes.card}>
                                    <img width={"152.4px"} height={"200px"} src={"/assets/nose.svg"}/>
                                    <h3>گوش</h3>
                                    <p>
                                        {post.nose && <ReactMarkdown source={post.nose}/>}

                                    </p>
                                    <Button variant={"outlined"} color={"primary"}>
                                        مشاهده بیشتر
                                    </Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Container>
                </div>
                <Divider/>
                <Container>
                    <div className={classes.question}>
                        <Grid container justify={"center"} alignItems={"center"}>
                            <Grid item xs={12}>
                                <img src="/assets/question.svg"/>
                            </Grid>
                            <Grid item xs={12}>
                                <h2>{post.questiontitle}</h2>
                                <p>
                                    {post.question && ReactHtmlParser(post.question)}
                                </p>
                                <Button variant={"outlined"} color={"primary"}>
                                    پرسیدن سوال
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
                <Divider/>
                <div style={{backgroundColor: '#f2f2f2',paddingBottom:16}}>
                <Container>
                    <div className={classes.question}>
                        <Grid container justify={"center"} alignItems={"center"}>
                            <Grid item xs={12}>
                                <img style={{objectFit: 'contain'}} src="/assets/computer.svg"/>
                            </Grid>
                            <Grid item xs={12}>
                                <h2>{post.reservetitle}</h2>
                                <p>
                                    {post.reserve && ReactHtmlParser(post.reserve)}
                                </p>
                                <Button variant={"outlined"} color={"primary"}>
                                    پرسیدن سوال
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
                </div>
                <Divider/>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
