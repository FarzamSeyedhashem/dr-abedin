/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import DateInput from "../components/DateInput";
import Button from "@material-ui/core/Button";
import {API} from "../config";
import {lang} from '../lang';

const styles = theme => ({
    root:{
        padding:'58px 0px',
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 1.38,
        letterSpacing: 'normal',
        textAlign: 'left',
        color: '#123574',
    },
    descr: {
        fontSize: 18,
        fontWeight: 300,
        lineHeight: 1.88,
    }
});

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            date:null,
            success:false,
        }
    }

    handleChangeDate = (e)=>{
        this.setState({
            date:e._d
        })
        // console.log(e._d)
    }

    componentDidMount() {

    }

    render() {
        const {classes,langName} = this.props;
        const signup = () => {
            const e = document.getElementById("gender");
            const strUser = e.options[e.selectedIndex].value;
console.log(strUser)
            var data = JSON.stringify({
                "full_name": document.getElementById('name').value + " " + document.getElementById('surname').value,
                "phone": document.getElementById('phone').value,
                "gender": strUser,
                "old": document.getElementById('age').value,
                'reserve_time':this.state.date
            });
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function m() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200 || xhr.status === 201) {
this.setState({success:true})
                    } else{

                    }
                }
            }.bind(this);
            xhr.open("POST", API+"/reserves");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
            xhr.send(data);
        };
        return (
            <div className={classes.root}>
                <Container>
                    <h1 className={classes.title}>
                        نوبت دهی آنلاین
                    </h1>
                    <p className={classes.descr}>
                        شما با پر کردن گزینه های زیر زمان پیشنهادی خود را برای حضور در مطب دکتر بهزاد عابدین به اطلاع
                        منشی میرسانید و در کمترین زمان ممکن برای تایید زمان ثبت شده با شما تماس حاصل می شود
                    </p>
                </Container>
                <Container maxWidth={"sm"}>
                    <Grid container spacing={4}>
                        <Grid item md={6} xs={12}>
                            <TextField style={{width: '100%'}} id="name"
                                       label={lang[langName].name} variant="outlined"/>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField style={{width: '100%'}} id="surname"
                                       label={lang[langName].family} variant="outlined"/>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField style={{width: '100%'}} id="phone"
                                       label={lang[langName].phone} variant="outlined"/>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <FormControl fullWidth variant="outlined" className={classes.formControl}>
                                <InputLabel htmlFor="outlined-age-native-simple">{lang[langName].gender}</InputLabel>
                                <Select
                                    autoWidth
                                    native
                                    label={lang[langName].gender}
                                    inputProps={{
                                        name: 'age',
                                        id: 'gender',
                                    }}
                                >
                                    <option value={'مرد'}>{lang[langName].man}</option>
                                    <option value={'زن'}>{lang[langName].woman}</option>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField type={'number'} style={{width: '100%'}} id="age"
                                       label={lang[langName].age} variant="outlined"/>
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <DateInput handleChangeDate={this.handleChangeDate}/>
                        </Grid>
                        <Grid item xs={12}>
                            {!this.state.success?<Button disabled={!this.state.date} onClick={signup}
                                     style={{padding: '14px 0px', borderRadius: 5, fontSize: 14, fontWeight: 'bold'}}
                                     fullWidth variant={"contained"} color={"primary"}>ثبت</Button>
                               : <Button disabled={!this.state.date} onClick={signup} style={{
                                padding: '14px 0px',
                                borderRadius: 5,
                                    backgroundColor:'#059669',
                                fontSize: 14,
                                fontWeight: 'bold'
                            }} fullWidth variant={"contained"} color={"primary"}>ثبت</Button>
                            }</Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

Index.propTypes = {
    classes: PropTypes.object.isRequired
};


export default withStyles(styles)(Index);
