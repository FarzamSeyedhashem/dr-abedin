import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import BlogCard from '../../components/blog_card/Index'
import Container from "@material-ui/core/Container";

const styles = theme => ({
    root: {
        '&>h1': {
            fontFamily: 'Shabnamm',
            fontSize: 24,
            marginTop: 24,
            marginBottom: 24,
            fontWeight: 'bold',
            lineHeight: 1.38,
            letterSpacing: 'normal',
        }
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };
    componentDidMount() {
        console.log(this.props.posts)
    }


    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Container>
                    <h1>بلاگ و مقالات</h1>
                    <Grid container spacing={2}>

                        {this.props.posts.map(item=><Grid item md={3} xs={12}>
                            <BlogCard post={item}/>
                        </Grid>)}
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
