import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from "@material-ui/core/Container";
import {Image} from '../../../config';

const styles = theme => ({
    root: {
        '&>div': {
            '&>h1': {
                marginTop: 64,
                fontFamily: 'Shabnam',
                fontSize: 32,
                fontWeight: 'bold',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.38,
                letterSpacing: 'normal',
                color: '#123574',
                marginBottom: 38,
            },
            '&>div': {
                '&>div': {
                    '&>img': {
                        width: '100%',
                        height: 'auto',
                        objectFit: 'cover',
                        borderRadius: 8,
                        // border: 'solid 1px #707070',

                    }
                }
            }
        }
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes,gallery} = this.props;
        return (
            <div className={classes.root}>
                <Container>
                    <h1>گالری تصاویر</h1>
                    <Grid container>

                        {gallery.map(item=><Grid item md={3} xs={12}>
                            <img src={Image + item.image.url}/>
                        </Grid>)}
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
