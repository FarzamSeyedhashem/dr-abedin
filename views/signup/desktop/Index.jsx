import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";


const styles = theme => ({
    root: {},
    signinCard: {
        '&>h1': {
            marginTop: 106,
            marginBottom: 24,
            fontFamily: 'Shabnam',
            fontSize: 30,
            fontWeight: 'bold',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.37,
            color: '#123574',
            letterSpacing: 'normal',
        },
        '&>a': {
            textDecoration: 'none',
            fontFamily: 'Shabnam',
            fontSize: 18,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.39,
            letterSpacing: 'normal',
            color: '#123574',
        },
        '&>button': {
            fontWeight: 'bold',
            fontSize: 14,
            color: '#fff',
        }
    }
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Container>

                    <Grid container justify="center" alignItems="center">
                        <Grid item md={4} xs={12}>
                            <div className={classes.signinCard}>
                                <h1>ثبت نام در سایت</h1>
                                <TextField style={{width: '100%', marginTop: 16, marginBottom: 16,}} id="outlined-basic"
                                           label="نام" variant="outlined"/>
                                <TextField style={{width: '100%', marginTop: 16, marginBottom: 16,}} id="outlined-basic"
                                           label="نام خانوادگی" variant="outlined"/>
                                <TextField style={{width: '100%', marginTop: 16, marginBottom: 16,}} id="outlined-basic"
                                           label="تلفن همراه" variant="outlined"/>
                                <TextField style={{width: '100%'}} id="outlined-basic" label="ایمیل"
                                           variant="outlined"/>

                                <TextField style={{width: '100%', marginTop: 16, marginBottom: 16,}} id="outlined-basic"
                                           label="رمز ورود" variant="outlined"/>
                                <TextField style={{width: '100%', marginTop: 16, marginBottom: 16,}} id="outlined-basic"
                                           label="تکرار رمز ورود" variant="outlined"/>
                                <Button
                                    style={{backgroundColor: '#123574', marginBottom: 20, width: '100%'}}>ورود</Button>
                                <a href={"/signin"}>قبلا در سایت ثبت نام کرده‌ام ؟ ورود به سایت</a>
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
