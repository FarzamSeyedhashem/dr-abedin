import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {Container} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import {Image} from "../../../config";
import ReactMarkdown from "react-markdown";


const styles = theme => ({
    root: {
        '&>img': {
            height: '220px',
            objectFit: 'cover',
            width: '100%',

        },
        '&>div': {
            '&>h1': {
                margin: 0,
                marginTop: 24,
                marginBottom: 12,
                fontFamily: 'Shabnam',
                fontSize: 18,
                fontWeight: 'bold',
                lineHeight: 1.67,
                color: '#212121'

            },
            '&>h6': {
                fontFamily: 'Shabnam',
                fontSize: 14,
                fontWeight: 'normal',
                lineHeight: 1.36,
                letterSpacing: -0.5,
                color: '#757575'

            },
            '&>p': {
                margin: 0,
                fontFamily: 'Shabnam',
                fontSize: 16,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 2,
                letterSpacing: 'normal',
                color: '#212121',

            }
        }
    },
    such: {
        marginBottom: 32,
        '&>h2': {
            marginTop: 24,
            marginBottom: 12,
            fontFamily: 'Shabnam',
            fontSize: 18,
            fontWeight: 'bold',
            lineHeight: 1.39,
            letterSpacing: 'normal',
        },
        '&>img': {
            height: 220,
            objectFit: 'contain',
            width: '100%',
            borderRadius: 4,
            marginBottom: 8,

        },
        '&>h3': {
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 'bold',
            lineHeight: 1.38,
            letterSpacing: 'normal',
            margin: 0,
            marginBottom: 8,
        },
        '&>h6': {
            fontFamily: 'Shabnam',
            fontSize: 12,
            fontWeight: 'normal',
            lineHeight: 1.33,
            letterSpacing: -0.43,
            margin: 0,
        }
    }
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes,post} = this.props;
        return (
            <div className={classes.root}>
                <img src={Image+this.props.post.thumbnail.url}/>
                <Container>
                    <h1>
                        {post.title}
                    </h1>
                    <h6>چهارشنبه | 23 خرداد</h6>
                    <p>
                        <ReactMarkdown source={post.des} />
                    </p>
                </Container>
                <Divider/>
                <Container>
                    <div className={classes.such}>
                        <h2>مطالب مرتبط</h2>
                        <img src="/assets/main-top.jpg"/>
                        <h3>اخذ شهروندی</h3>
                        <h6>چهارشنبه | 23 خرداد</h6>
                    </div>

                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
