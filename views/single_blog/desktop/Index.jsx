import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {Container} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import {Image} from '../../../config'
const ReactMarkdown = require('react-markdown')

const styles = theme => ({
    root: {
        '&>div': {
            '&>h1': {
                marginTop: 34,
                marginBottom: 34,
                fontFamily: 'Shabnam',
                fontSize: 36,
                fontWeight: 'bold',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.36,
                letterSpacing: 'normal',
                color: '#123574',

            },
            '&>div': {

                '&>div': {
                    '&>h2': {
                        fontFamily: 'Shabnam',
                        fontSize: 18,
                        fontWeight: 'bold',

                        lineHeight: 1.39,

                        color: '#123574',
                    },

                    '&>img': {
                        width: '100%',
                        borderRadius: 8,
                        objectFit: 'cover'
                    },
                    '&>p': {
                        fontFamily: 'Shabnam',
                        fontSize: 16,
                        fontWeight: 'normal',
                        fontStretch: 'normal',
                        fontStyle: 'normal',
                        lineHeight: 1.88,
                        letterSpacing: 'normal',
                        color: '#212121'
                    },

                }
            }
        }
    },
    archive: {
        '&>img': {
            width: '100%',
            borderRadius: 8,
            objectFit: 'cover'
        },
        '&>p': {
            marginTop: 16,
            marginBottom: 8,
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.88,
            letterSpacing: 'normal',
            color: '#212121'
        },
        '&>h3': {
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 'bold',
            margin: 0,
            lineHeight: 1.38,
            marginTop: 12,
            marginBottom: 12,
            color: '#212121',
        },
        '&>a': {
            marginBottom:16,
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            lineHeight: 1.36,
            color: '#747474',
        }
    },

});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes,post} = this.props;
        return (
            <div className={classes.root}>
                <Container>
                    <h1>{post.title}</h1>
                    <Grid container justify={"space-between"} spacing={8}>
                        <Grid item md={8}>

                            <img height="472px" src={Image+this.props.post.thumbnail.url}/>
                            <p>
                                <ReactMarkdown source={post.des} />
                            </p>
                        </Grid>
                        <Grid item md={3}>
                            <h2>دیگر مطالب بلاگ</h2>
                            <div className={classes.archive}>
                                <img height="160px" src="/assets/main-top.jpg"/>
                                <h3>
                                    با رژیم غذایی گوش درد خود را درمان کنید
                                </h3>
                                <p>ورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</p>
                                <a>
                                    مشاهده بیشتر
                                </a>
                                <Divider/>
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
