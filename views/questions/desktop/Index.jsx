import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {Container} from "@material-ui/core";
import ExpansionPanel from "../../../components/ExpansionPanel/Index"

const styles = theme => ({
    root: {
        '&>div': {
            '&>h1': {
                [theme.breakpoints.up('md')]: {
                    marginTop: 34,
                },
                marginTop: 84,
                marginBottom: 34,
                fontFamily: 'Shabnam',
                fontSize: 32,
                fontWeight: 'bold',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.38,
                letterSpacing: 'normal',
                color: '#123574',
            }
        }
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes, posts,lang} = this.props;
        const post = posts.filter(item => item.show && item.lang && item.lang.name === lang);
        return (
            <div className={classes.root}>
                <Container>
                    <h1>سوالات متداول</h1>
                    <ExpansionPanel posts={post}/>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
