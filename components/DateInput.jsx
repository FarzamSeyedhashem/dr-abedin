import moment from "moment";
import jMoment from "moment-jalaali";
import React, { useState } from "react";
import JalaliUtils from "@date-io/jalaali";
import {
    TimePicker,
    DateTimePicker,
    DatePicker,
    MuiPickersUtilsProvider, KeyboardDatePicker, KeyboardDateTimePicker,
} from "@material-ui/pickers";

jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });

function PersianExample(props) {
    const [selectedDate, handleDateChange] = useState(moment());
    const handleChangeDate = e => {
        props.handleChangeDate(e)
        handleDateChange(e)
    }
    return (
        <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
            <KeyboardDateTimePicker
                style={{width:'100%'}}
                autoOk
                variant="inline"
                inputVariant="outlined"
                okLabel="تأیید"
                cancelLabel="لغو"
                // labelFunc={date => (date ? date.format("jYYYY/jMM/jDD") : "")}
                value={selectedDate}
                onChange={handleChangeDate}
            />
        </MuiPickersUtilsProvider>
    );
}

export default PersianExample;
