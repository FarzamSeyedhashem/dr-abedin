import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& .MuiInputLabel-root':{
            display:'none'
        },
        '& .MuiOutlinedInput-notchedOutline':{
            display:'none'
        },
        '& .MuiOutlinedInput-input':{
            border:'solid 1px rgba(0, 0, 0, 0.12)',
            borderRadius:8,
        },
        '& .MuiInputBase-input':{
            fontFamily:'Shabnam !important',
            fontSize:14,
        },
        '& .MuiTextField-root':{
            margin:0,
        }
    },
    textField: {
        // marginLeft: theme.spacing(1),
        // marginRight: theme.spacing(1),
        width: '100%',
    },
}));

function TextFields(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState('');
    const handleChange = event => {
        setValue(event.target.value);
        props.validator&&props.validator(event.target.value)
    };
    return (
        <div className={classes.root}>
            <TextField
                autoFocus={props.autoFocus?true:false}
                onChange={handleChange}
                id={props.id}
                label="Label"
                value={value}
                placeholder={props.placeHolder}
                fullWidth
                type={props.type}
                margin="dense"
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
            />
        </div>
    );
}
export default TextFields;
