import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import Input from './Input'
import LocationOnIcon from '@material-ui/icons/LocationOn';
import EmailIcon from '@material-ui/icons/Email';
import PhoneEnabledRoundedIcon from '@material-ui/icons/PhoneEnabledRounded';

const styles = theme => ({
    root: {},
    topFooter: {
        backgroundColor: '#d4d4d4',
        paddingTop: 17,
        paddingBottom: 17,

        textAlign: 'center',
        fontFamily: 'Shabnam',
        fontSize: 16,
        fontWeight: 'normal',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 1.38,
        letterSpacing: 'normal',
        alignItems: 'center',
        '&>span': {
            height: 22,
        }
    },
    menu: {
        listStyle: 'none',
        padding: 0,
        borderRight: '1px solid rgba(0,0,0,.1)',
        '&>li': {
            marginBottom: 26,
            '&>a': {
                textDecoration: 'none',
                fontFamily: 'Shabnam',
                fontSize: 16,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.38,
                letterSpacing: 'normal',
                color: '#212121',
            }
        }
    },
    email: {
        '&>h4': {
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.38,
            letterSpacing: 'normal',
            color: '#212121',
            marginTop:0,
            marginBottom:20,
        },
        '&>p':{
            fontFamily: 'Shabnam',
            fontSize: 14,
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 2.07,
            letterSpacing: 'normal',
            color: '#212121',
            margin:0,
            marginBottom:16,
        }
    },
    social:{
        listStyle: 'none',
        borderLeft: '1px solid rgba(0,0,0,.1)',
        '&>li': {
            marginBottom: 26,
            width:100, textAlign:'center',
            '&>a': {

                textDecoration: 'none',
                fontFamily: 'Shabnam',
                fontSize: 16,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.38,
                letterSpacing: 'normal',
                color: '#212121',
            }
        }
    }
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes} = this.props;
        return (
            <div>

                {/*<div className={classes.topFooter}>*/}
                {/*    <span>ساعت کاری دفتر شنبه تا چهارشنبه ۱۵ تا ۱۹ و ساعت پاسخگویی تلفن از ۱۴ تا ۱۹</span>*/}
                {/*</div>*/}
                <Container>
                    <div style={{background:'#fafafa',padding:"24px 16px",border:'1px solid rgba(0,0,0,.12)',marginTop:20,borderRadius:'24px',marginBottom:16}}>
                        <Grid container>
                            <Grid md="6" xs={12}>
                                <img style={{width:'100%',height:250,}} src="/time.svg"/>
                            </Grid>
                            <Grid md="6" xs={12}>
                                <h3 style={{fontWeight:'bold',fontSize:24}}>ساعات کاری</h3>
                                <p style={{fontWeight:'normal',fontSize:18,}}>شنبه تا چهارشنبه ۱۵ تا ۱۹</p>
                                <p style={{fontWeight:'normal',fontSize:18,}}>ساعت پاسخگویی تلفن از ۱۴ تا ۱۹</p>
                            </Grid>
                        </Grid>

                    </div>
                </Container>
                <Divider/>
                <Container>

                    <Grid style={{marginTop:24}} container justify={"space-between"} alignItems={"center"} spacing={4}>
                        <Grid item md={4}>
                            <ul className={classes.menu}>
                                <li>
                                    <a href={"/"}>
                                        صفحه اصلی
                                    </a>
                                </li>
                                <li>
                                    <a href={"/"}>
                                        نوبت دهی آنلاین
                                    </a>
                                </li>
                                <li>
                                    <a href="/blog">
                                        مقالات
                                    </a>
                                </li>
                                <li>
                                    <a href="/blog">
                                        گالری تصاویر
                                    </a>
                                </li>

                            </ul>
                        </Grid>
                        <Grid item md={4}>
                            <div className={classes.email}>
                                <h4>
                                    برای عضویت در خبرنامه ایمیل خود را وارد نمایید
                                </h4>
                                <p>
                                    با عضویت در خبرنامه سایت شما از اخبار و مقالاتی که در سایت قرار میگیرد از طریق ایمیل
                                    ثبت شده مطلع میشوید
                                </p>
                                <Input placeHolder={"ایمیل"}/>
                            </div>
                        </Grid>
                        <Grid item md={4}>
                            <ul className={classes.social}>
                                <li>
                                    <a href={"/"}>
                                        سوالات متداول
                                    </a>
                                </li>
                                <li>
                                    <a href={"/"}>
                                        تماس با ما
                                    </a>
                                </li>
                                <li>
                                    <a href="/blog">
                                        درباره ما
                                    </a>
                                </li>

                            </ul>
                        </Grid>
                        <Grid item md={6}>
                            <div style={{marginBottom:24,display:'flex',alignItems:'center',vertcalAlign:'middle',color:'#757575',fontSize:16}}>
                                <LocationOnIcon/>
                                <span style={{marginRight:16}}>ایران - تهران - شریعتی - بالاتر از پل صدر - روبروی سفارت واتیکان - ساختمان رسا - شماره 1730 - طبقه اول - واحد دو</span>
                            </div>
                            <div style={{marginBottom:24,display:'flex',alignItems:'center',vertcalAlign:'middle',color:'#757575',fontSize:16}}>
                                <EmailIcon/>
                                <span style={{marginRight:16}}>Info@dr-behzadabedin.com</span>
                            </div>
                            <div style={{marginBottom:24,display:'flex',alignItems:'center',vertcalAlign:'middle',color:'#757575',fontSize:16}}>
                                <PhoneEnabledRoundedIcon/>
                                <span style={{marginRight:16}}>۲۶۶۰۱۹۶۷</span>
                            </div>
                        </Grid>
                        <Grid item md={6}>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12946.160223145382!2d51.43575!3d35.786673!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4dc836b5234631e4!2z2K_aqdiq2LEg2KjZh9iy2KfYryDYudin2KjYr9uM2YY!5e0!3m2!1sen!2s!4v1600542970983!5m2!1sen!2s"
                                frameBorder="0" style={{border:0,width:'100%',height:400,borderRadius:16,}} allowFullScreen=""
                                aria-hidden="false" tabIndex="0"></iframe>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
