import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles/index';
import Grid from '@material-ui/core/Grid/index';
import Divider from "@material-ui/core/Divider";
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import {Image} from '../../config'
import Button from "@material-ui/core/Button";

const styles = theme => ({
    root: {
        borderRadius: 8,
        border: 'solid 1px #e6e6e6',
        position: 'relative',
        paddingBottom: 51,
        '&>img': {
            width: '100%',
            height: 278,
            objectFit: 'cover',
        },
        '&>div': {
            paddingLeft: 16,

            '&>h3': {
                marginTop: 16,
                marginBottom: 8,
                fontFamily: 'Shabnam',
                fontSize: 18,
                fontWeight: 'bold',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.39,
                letterSpacing: 'normal',
                color: '#212121',
            },
            '&>h6': {
                margin: 0,
                marginBottom: 16,
                fontFamily: 'Shabnam',
                fontSize: 14,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 2.21,
                letterSpacing: 'normal',
                color: '#757575',
            },
            '&>p': {
                // maxWidth:238,
                paddingRight: 24,
                fontFamily: 'Shabnam',
                fontSize: 14,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.86,
                letterSpacing: 'normal',
                color: '#757575',
            },
            '&>a': {
                position: 'absolute',
                bottom: 16,
                left: 16,
                display: 'flex',
                fontFamily: 'Shabnam',
                fontSize: 14,
                fontWeight: 'normal',
                fontStretch: 'normal',
                fontStyle: 'normal',
                lineHeight: 1.36,
                letterSpacing: 'normal',
                color: '#123574',
            }
        }
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };

    componentDidMount() {

    }


    render() {
        const {classes, post} = this.props;
        return (
            <div className={classes.root}>
                <img src={Image + post.thumbnail.url}/>
                <div>
                    <h3>
                        {post.title}
                    </h3>
                    {/*<h6>پنجشنبه 23 اسفند</h6>*/}
                    <Divider/>
                    <p>
                        {post.summery}
                    </p>
                    <Button style={{display: 'flex'}} variant={"outlined"} color={"primary"} href={"blog/" + post.id}>
                        مشاهده بیشتر
                        <NavigateBeforeIcon/>
                    </Button>
                </div>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
