import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ReactMarkdown from "react-markdown";
import {API} from '../../config';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& .MuiExpansionPanelDetails-root': {
            fontFamily: 'Shabnam',
            fontSize: 16,
            fontWeight: 'normal',

            color: '#545454',

        },
        '& .MuiExpansionPanelSummary-root': {
            borderRadius: 8,
            height: 25,
            boxShadow: '0px 0px 0px 0px',
            // border: 'solid 1px #e6e6e6',
            paddingTop: 27,
            paddingBottom: 27,
        },
        '& .Mui-expanded': {
            border: 'solid 0px #e6e6e6',
            paddingTop: 16,

            '&>div': {
                '&>p': {
                    fontFamily: 'Shabnam',
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#123574',
                    // paddingBottom: 24,
                    paddingBottom: 24,
                    borderBottom: '1px solid rgba(0,0,0,.1)'
                }
            }
        }
    },
    heading: {
        width: '100%',
        fontSize: theme.typography.pxToRem(15),
        // flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));

export default function ControlledExpansionPanels(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    const signup = () => {

        var data = JSON.stringify({
            "title": document.getElementById('title').value,
            "phone": document.getElementById('phone').value,
            "name": document.getElementById('name').value,
            "email": document.getElementById('email').value,
        });
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function m() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200 || xhr.status === 201) {

                } else{

                }
            }
        }.bind(this);
        xhr.open("POST", API+"/questions");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.send(data);
    };
    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                {props.posts.map((item,num)=><Grid item md={12} xs={12}>
                    <ExpansionPanel expanded={expanded === num} onChange={handleChange(num)}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                        >
                            <Typography className={classes.heading}>
                                {item.title}
                            </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Typography>
                                <ReactMarkdown source={item.question} />
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </Grid>)}
            </Grid>
            <Grid container spacing={2} style={{marginTop: 32,}}>
                <Grid item md={12}>
                    <Divider/>
                </Grid>
                <Grid item md={4} xs={12}>
                    <TextField style={{width: '100%', borderRadius: 8,}} id="name"
                               label="نام و نام خانوادگی" variant="outlined"/>
                </Grid>
                <Grid item md={4} xs={12}>
                    <TextField style={{width: '100%', borderRadius: 8,}} id="email"
                               label="ایمیل" variant="outlined"/>
                </Grid>
                <Grid item md={4} xs={12}>
                    <TextField style={{width: '100%', borderRadius: 8,}} id="phone"
                               label="تلفن همراه" variant="outlined"/>
                </Grid>
                <Grid item md={12} xs={12}>
                    <TextField rows="4" multiline style={{width: '100%', borderRadius: 8,}} id="title"
                               label="سوال شما" variant="outlined"/>
                </Grid>
            </Grid>
            <Button onClick={signup} color={"primary"} variant={"contained"} style={{marginTop: 16,}}>ارسال</Button>

        </div>
    );
}
