import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import Drawer from './Drawer';
import IconButton from "@material-ui/core/IconButton";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import InstagramIcon from "@material-ui/icons/Instagram";

const styles = theme => ({
    root: {
        boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.3)',
        backgroundColor: '#f5f5f5',
        height: 56,
        position: 'fixed',
        top: 0,
        width: '100%',
        zIndex:999,
    },
});

class LearnCard extends React.Component {

    state = {};

    handleExpandClick = () => {

    };
    account = () => {
        window.location.href= "/signin"
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Container>
                    <Grid style={{height:56,}} container justify={"space-between"} alignItems="center">
                        <Grid item md={"auto"}>
                            <Drawer/>
                        </Grid>
                        <Grid md={"auto"}>
                            <div style={{display:'flex'}}>
                                <Button style={{borderRadius:'99px',  FontFamily:'Billabong !important',background:'linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d)',
                                }}>
                                    <a style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        textDecoration: 'none',
                                        color:'#fff',
                                        FontFamily:'Billabong !important'
                                        // color: 'transparent',
                                    }} href="https://instagram.com/dr.behzadabedin?igshid=l7gydzh3shom">
                                        Instagram
                                        <InstagramIcon style={{marginRight: 8,}}/>
                                    </a>
                                </Button>
                            {/*<IconButton>*/}
                            {/*    <SearchOutlinedIcon/>*/}
                            {/*</IconButton>*/}
                            {/*<IconButton onClick={this.account}>*/}
                            {/*    <AccountCircleOutlinedIcon/>*/}
                            {/*</IconButton>*/}
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

LearnCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LearnCard);
