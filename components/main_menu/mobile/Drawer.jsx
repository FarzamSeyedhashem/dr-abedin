import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
});

export default function SwipeableTemporaryDrawer() {
    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({...state, [side]: open});
    };

    const sideList = side => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                <div style={{padding:16}}>
                <img style={{height:36, objectFit:'contain'}} src={"/logo.jpg"}/>
                </div>
                <Divider/>
                <ListItem button component={"a"} href={"/"}>
                    <ListItemText primary={"صفحه اصلی"}/>
                </ListItem>
                <ListItem button component={"a"} href={"/reserve"}>
                    <ListItemText primary={"رزرو آنلاین"}/>
                </ListItem>
                <ListItem button component={"a"} href={"/blog"}>
                    <ListItemText primary={"مقالات"}/>
                </ListItem>
                <ListItem button component={"a"} href={"/gallery"}>
                    <ListItemText primary={"گالری تصاویر"}/>
                </ListItem>
                <ListItem button component={"a"} href={"/questions"}>
                    <ListItemText primary={"سوالات متداول"}/>
                </ListItem>
            </List>
        </div>
    );

    return (
        <div>
            <IconButton onClick={toggleDrawer('left', true)}><MenuIcon/></IconButton>
            <SwipeableDrawer
                anchor="left"
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer>
        </div>
    );
}
