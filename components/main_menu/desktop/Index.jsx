import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Container} from "@material-ui/core";
import PersonIcon from '@material-ui/icons/Person';
import Grid from "@material-ui/core/Grid";
import Tabs from "./Tabs"
import InstagramIcon from '@material-ui/icons/Instagram'

const useStyles = makeStyles(theme => ({
    root: {
        position: 'fixed',
        width: '100%',
        flexGrow: 1,
        zIndex: 999,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    topBar: {
        backgroundColor: '#f7f7f7',

        // height: 40,
        '&>div': {

            display: 'flex',
            '&>div:nth-child(1)': {
                flexGrow: 1,
            },
            '&>div': {
                display: 'flex',
                paddingTop: 11,
                paddingBottom: 11,
                '&>svg': {
                    height: 18,
                    width: 18,
                    fill: '#757575'
                },
                '&>a': {
                    fontFamily: 'Shabnam',
                    fontSize: 14,
                    fontWeight: 'normal',
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.36,
                    letterSpacing: 'normal',
                    color: '#424242',
                    marginLeft: 8,
                },

                '&>span': {
                    fontFamily: 'Shabnam',
                    fontSize: 14,
                    fontWeight: 300,
                    fontStretch: "normal",
                    fontStyle: 'normal',
                    lineHeight: 1.36,
                    letterSpacing: 'normal',
                    color: '#727272',
                    marginRight: 6,
                    marginLeft: 6,
                }
            }
            // paddingTop:11,
            // paddingBottom:11,
        }

    },
    appbar: {
        backgroundColor: '#ffffff',
        boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.12)'
    },
    toolbar: {},
    logo: {
        marginRight: 24,
        width: 116,
        height: 36,
        objectFit: 'contain',

    },
    insta: {
        WebkitBackgroundClip: 'text',
        WebkitTextFillColor: 'transparent',
        mozBackgroundClip: 'text',
        mozTextFillColor: 'transparent',
    },
    decoration: {
        display:'block',
        height:'fit-content',
        textDecoration: 'none'
    }
}));

export default function ButtonAppBar(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.topBar}>
                <Container>
                    <div>
                        {/*<PersonIcon/>*/}
                        {/*<a>ثبت نام</a>*/}
                        {/*<span>یا</span>*/}
                        {/*<a>ورود</a>*/}
                        <Button style={{background:'linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d)',
                        }}>
                        <a style={{
                            display: 'flex',
                            alignItems: 'center',
                            textDecoration: 'none',
                            color:'#fff',
                            FontFamily:'Billabong !important'
                            // color: 'transparent',
                        }} href="https://instagram.com/dr.behzadabedin?igshid=l7gydzh3shom">
                           Instagram
                            <InstagramIcon style={{marginRight: 8,}}/>

                        </a>
                        </Button>
                    </div>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                    }}>
                        {props.lang !== 'ar' && <a className={classes.decoration} href={"/ar"}>العربی</a>}
                        {props.lang !== 'kr' && <a className={classes.decoration} href={'/kr'}>کردی</a>}
                        {props.lang !== 'tr' && <a className={classes.decoration} href={'/tr'}>ترکی</a>}
                        {props.lang !== 'en' && <a className={classes.decoration} href={'/en'}>English</a>}
                        {props.lang !== 'fa' && <a className={classes.decoration} href={'/fa'}>فارسی</a>}
                    </div>
                </Container>
            </div>
            <AppBar className={classes.appbar} position="static">
                <Toolbar className={classes.toolbar}>
                    <Container>
                        <Grid container alignItems={"center"}>
                            <img className={classes.logo} src={"/assets/logo.png"}/>
                            <Tabs lang={props.lang}/>
                            <Button color="inherit">Login</Button>
                        </Grid>
                    </Container>
                </Toolbar>
            </AppBar>
        </div>
    );
}
