import React from 'react';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles({
    root: {


        boxShadow: '0px 0px 0px 0px',
        backgroundColor: 'transparent'
    },
    tabs: {

        overflow: 'visible !important',
        height: '100%',
        boxShadow: '0px 0px 0px 0px',
        border: 0,
        backgroundColor: 'transparent',
        '&$tabSelected': {
            color: '#fff',
        }, '&:focus': {
            color: '#fff',
        },
    },
    tab: {

        marginRight: 24,
        height: '100%',
        '&:div': {
            height: '100%',
            '&:div': {
                height: '100%'
            }
        },

        textTransform: 'none',
        minWidth: 'fit-content',
        // width: 'fit-content',
        letterSpacing: 0,
        color: 'rgba(0,0,0,0.5)',
        '&$tabSelected': {
            color: '#fff',
        },

    },
    flexContainer: {
        height: '100%'
    },
    tabsIndicator: {

        boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.15)',
        top: 54,
    },
    labelIcon: {
        fontFamily: 'Shabnam',
        fontSize: 14,
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 1.39,
        letterSpacing: 'normal',
        textAlign: 'right',
        padding: '0px 8px',
        // paddingTop: 2,

        '&>span': {
            '&>svg': {
                marginBottom: '28px !important',
            }
        }

    },
    scroller: {
        overflow: 'visible !important',
        overflowX: 'visible !important'
    },
    selected: {
        fontFamily: 'Shabnam',
        fontSize: 14,
        fontWeight: 'bold',
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 1.36,
        letterSpacing: 'norm'
    }

});
const routes = [
    {path:'',name:'صفحه اصلی'},
    {path:'/reserve',name:'نوبت‌دهی آنلاین'},
    {path:'/services',name:'خدمات'},
    {path:'/blog',name:'مقالات'},
    {path:'/gallery',name:'گالری تصاویر'},
    {path:'/questions',name:'سوالات متداول'},
];
export default function IconLabelTabs(props) {
    console.log(window.location.pathname)
    var index;
    {
        routes.map((route, i) => {
            if ("/"+props.lang+route.path === window.location.pathname || "/"+props.lang+route.path+'/' === window.location.pathname) {

                index = i;
            }

        })
    }
    const classes = useStyles();
    const [value, setValue] = React.useState(index);

    function handleChange(event, newValue) {
        setValue(newValue);
    }

    return (
        <Paper square className={classes.root}>
            {/*{console.log(props.routes)}*/}
            <Tabs
                classes={{
                    root: classes.tabs,
                    indicator: classes.tabsIndicator,
                    flexContainer: classes.flexContainer,
                    scroller: classes.scroller
                }}
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
            >
                {routes.map((route)=>
                    <Tab disableFocusRipple disableRipple component="a" href={"/"+props.lang+route.path} classes={{
                        root: classes.tab,
                        labelIcon: classes.labelIcon,
                        labelContainer: classes.labelContainer,
                        wrapper: classes.wrapper,
                        selected: classes.selected,
                    }} label={route.name}/>
                )}
            </Tabs>
        </Paper>
    );
}
