import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {Hidden} from "@material-ui/core";
import DesktopPage from "./desktop/Index";
import MobilePage from "./mobile/Index";
import {withRouter} from "next/router";

// import Layout from "../../layouts/Main";

// const styles = theme => ({
//     root: {},
// });

class Main extends React.Component {

    state = {};

    handleExpandClick = () => {

    };


    render() {
        const {classes} = this.props;
        return (
            <div>
                <Hidden smDown>
                    <DesktopPage lang={this.props.router.query.lang}/>
                </Hidden>
                <Hidden mdUp>
                    <MobilePage lang={this.props.router.query.lang}/>
                </Hidden>
            </div>
        );
    }
}

Main.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRouter(Main);
