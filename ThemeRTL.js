import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#123574',
        },
        secondary: {
            main: '#212121',
        },
        error: {
            main: red.A400,
        },
        background: {
            default: '#fff',
        },
    },
    typography: {
        fontFamily:['Shabnam','Yekan'].join(','),
        // Use the system font instead of the default Roboto font.
        // fontFamily: 'Lato',
    },
    direction: 'rtl',

});

export default theme;
