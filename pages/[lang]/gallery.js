import React from 'react'
import Layout from "../../layouts/Main";
import GalleryPage from "../../views/image_gallery/Index"
import {API} from "../../config";

export async function getServerSideProps(context) {
    const getBlogPost = await fetch(API + '/galleries');
    const gallery = await getBlogPost.json();

    return {
        props: {
            gallery
        }
    }
}

export default function Home({gallery}) {
    return (
        <Layout>
            <GalleryPage gallery={gallery}/>
        </Layout>
    )
}