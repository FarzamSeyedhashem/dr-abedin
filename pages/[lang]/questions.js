import React from 'react'
import Layout from "../../layouts/Main";
import QuestionsPage from "../../views/questions/Index"
import {API} from "../../config";

export async function getServerSideProps(context) {
    const getBlogPost = await fetch(API + '/questions');
    const posts = await getBlogPost.json();
    const lang = context.query.lang
    return {
        props: {
            lang,
            posts
        }
    }
}

export default function Home({posts,lang}) {
    return (
        <Layout>
            <QuestionsPage lang={lang} posts={posts}/>
        </Layout>
    )
}


