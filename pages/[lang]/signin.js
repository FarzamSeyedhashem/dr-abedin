import React from 'react'
import Layout from "../../layouts/Main";
import MainPage from "../../views/signin/Index"

const Home = () => (
    <Layout>
        <MainPage/>
    </Layout>
)

export default Home
