import React from 'react'
import Layout from "../../layouts/Main";
import MainPage from "../../views/main/Index"
import {API} from "../../config";

export async function getServerSideProps(context) {
    const getBlogPost = await fetch(API + '/mainpage');
    const posts = await getBlogPost.json();
    const lang = context.query.lang
    return {
        props: {
            lang,
            posts
        }
    }
}

export default function Home({posts,lang}) {
    return (

        <Layout>
            <MainPage lang={lang} posts={posts}/>
        </Layout>
    )
}