import React from 'react'
import Layout from "../../layouts/Main";
import MainPage from "../../views/reserve"
import {API} from "../../config";
export async function getServerSideProps(context) {
    const lang = context.query.lang
    return {
        props: {
            lang,
        }
    }
}

export default function Home({lang}) {
    return (

        <Layout>
            <MainPage langName={lang}/>
        </Layout>
    )
}