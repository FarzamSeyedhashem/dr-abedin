import React from 'react'
import Layout from "../../../layouts/Main";
import MainPage from "../../../views/single_blog/Index"
import {API} from '../../../config'

export async function getServerSideProps(context) {
    const getBlogPost = await fetch(API + '/blogs/'+context.params.id);
    const posts = await getBlogPost.json();

    return {
        props: {
            posts
        }
    }
}

export default function Home({posts}) {
    return (
        <Layout>
            <MainPage post={posts}/>
        </Layout>
    )
}
