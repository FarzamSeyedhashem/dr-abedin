import React from 'react'
import Layout from "../../../layouts/Main";
import BlogPage from "../../../views/blog/Index"
import {API} from '../../../config'

export async function getServerSideProps() {
    const getBlogPost = await fetch(API + '/blogs');
    const posts = await getBlogPost.json();

    return {
        props: {
            posts
        }
    }
}

export default function Home({posts}) {
    return (
        <Layout>
            <BlogPage posts={posts}/>
        </Layout>
    )
}
