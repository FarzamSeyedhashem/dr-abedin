import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../Theme';
import {ThemeProvider} from '@material-ui/core/styles';
import {StylesProvider, jssPreset} from '@material-ui/core/styles';
import rtl from 'jss-rtl';
import {create} from 'jss';
import '../public/Index.css';
import 'swiper/css/swiper.min.css';
import { withRouter } from 'next/router'
import ThemeRTL from '../ThemeRTL'
class MyApp extends App {
    componentDidMount() {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }
    constructor(props) {
        super(props);
        console.log(this.props.router.query)
    }
    render() {
        const {Component, pageProps} = this.props;
        const jss = create({plugins: [...jssPreset().plugins, rtl()]});

        return (
            this.props.router.query.lang&&this.props.router.query.lang=='fa'?
            <ThemeProvider theme={ThemeRTL}>
                <div  dir="rtl">
                <StylesProvider jss={jss}>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline/>
                    <Component {...pageProps} />
                </StylesProvider>
                </div>
            </ThemeProvider>:<ThemeProvider theme={theme}>

                        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}

                        <Component {...pageProps} />

                </ThemeProvider>

        );
    }
}
export default withRouter(MyApp);