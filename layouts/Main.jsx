import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Footer from "../components/footer/Index.jsx";
import Menu from "../components/main_menu/Index.jsx";


const styles = theme => ({

    main: {
        [theme.breakpoints.up('md')]: {
            paddingTop: 105,
        },

        minHeight: 'calc(100vh)',
    }

});

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "blue",
        };
    }

    getRoute() {
        return this.props.location.pathname !== "/admin/maps";
    }

    handleColorClick = color => {
        this.setState({color: color});
    };

    render() {

        const {classes, ...rest} = this.props;
        return (


            <div className={classes.root}>
                <Menu {...rest}/>
                <main className={classes.main}>
                    <div>{this.props.children}</div>
                </main>
                <Footer/>
            </div>


        );
    }
}

Dashboard.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
